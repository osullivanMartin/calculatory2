package calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Algo
{
		static ArrayList<String> equation = new ArrayList<String>();

		public static void convert(String infix)
		{
			Stack<String> s = new Stack<String>();
			int i = 0;
			ArrayList<String> a = new ArrayList<String>(Arrays.asList(infix.split("") ) ); // Convert Infix to ArrayList
			
			
			while(i != a.size()-1) // Concatenate Numbers
			{
				if( isNum(a.get(i)) && isNum(a.get(i+1) ))
				{
					a.set(i, a.get(i) + a.get(i+1)); 
					a.remove(i+1);
					continue;
				}
				i++;
			}
			i=0;
			
			System.out.println("Pre minus concat : " + a.toString());
			
			while(i != a.size()-1) // Concatenate negative numbers with - 
			{
				if(a.get(i).equals("-") )
				{
					for(int j=i-1; j>0 ;j--)
					{
						if(isOper(a.get(j) ) )
						{
							a.set(i, a.get(i) + a.get(i+1) ); 
							a.remove(i+1);
							break;
						}
						else if(isNum(a.get(j) ) )
						{
							break;
						}
					}
					if(i == 0)
					{
						a.set(i, a.get(i) + a.get(i+1) ); 
						a.remove(i+1);
					}
				}
				i++;
			}
			
			System.out.println("Infix : " + a.toString() );
			
			for(i=0; i< a.size(); i++)
			{
				switch(a.get(i) )
				{
					case "(":
						s.push(a.get(i) );
						break;
						
					case ")":
						while (!s.isEmpty() ) 
		                {
					         if (s.peek().equals( "(" )) 
					         {
					        	 s.pop();
						         break;	 
					         }
					         else 
				        	 {
					        	 equation.add(s.pop() ); 
				        	 }
		                }
						break;
						
					case "+": case "-": case "*": case "/":
						 while (!s.isEmpty()) 
					      {
							 if (s.peek().equals("(") ) 
					         {
					            break;
					         } 
					         else 
					         {
					        	 if(pre(a.get(i) ) >= pre(s.peek() ) )
			        			 {
					                 break;
			        			 }
					        	 else 
					        	 { 
					        		 equation.add(s.pop() );
					        	 }
					         } 
					     }
						 s.push(a.get(i) );
			               break;
		                
					default:
						equation.add(a.get(i) );
				}
			}
			
			while (!s.isEmpty() ) 
			{
				equation.add(s.pop() );
		    }
			System.out.println("PostFix : " + equation.toString() );

			preConvert();
		}

		private static int pre(String s)
		{
	        switch (s) 
	        {
		        case "+": case "-":
		            return 1;
		
		        case "*": case "/":
		            return 2;
	        }
	        return -1;
	    }	
		
		public static boolean isNum(String s)
		{  
			try  
			{  
				 @SuppressWarnings("unused")
				double d = Double.parseDouble(s);  
			}  
			catch(NumberFormatException e)  
			{  
				return false;  
			}  
			return true;  
		}
		
		public static boolean isOper(String s)
		{  
			switch(s)
			{
				case "*": case "/": case "+": case "-":
				return true;
				
				default: 
				return false;
			}
		}
		
		public static double Calculate (double number1, String operator, double number2)
		{		
			double result=0;
			
				switch ( operator ) // finding operators (+,-,*,/) and numbers
				{	
					case "+":
						System.out.println("Operation: addition");
						result = number1 + number2;
						break;	
						
					case "-":
						System.out.println("Operation: substraction");
						result = number1 - number2;
						break;	
						
					case "*":
						System.out.println("Operation:  multiplication");
						result = number1 * number2;
						break;	
						
					case "/":
						System.out.println("Operation: division");
						result = number1 / number2;
						break;	
					
						default: 			
					
				} // end switch	finding operators (+,-,*,/)	and numbers
				
				return result;
		}	
	
		public static void preConvert()
		{
				//postfix.add(0,"hello");
				//postfix.remove("-3.0");
				//postfix.remove(0);
				//postfix.set(0, "Tom");
				//int pos = postfix.indexOf("2.0");	System.out.println(pos);
				//int numberofitems = postfix.size(); 
				// postfix.clear();
				// if (postfix.get(i).equals("4") ) 
				// postfix.get(i);
				
			while ( equation.size()!=1 )
			{
				int i=0; // start at the beginning of the array
				
				while ( !equation.get(i).equals("-") && !equation.get(i).equals("+") && !equation.get(i).equals("*") && !equation.get(i).equals("/")  ) // find closest operator (+,-,*,/)
				{
					i++;		
				}
				
			// ----------- display content of the current array ----------------
				System.out.println("---- Before ----");
				for (int a=0; a<equation.size(); a++) 
				{
					System.out.print( equation.get(a)+ "|");
				}
				System.out.println();
		   // ----------- end display content of the current array ----------------
				
				System.out.println("Numbers will be: "+equation.get(i-2) +", "+ equation.get(i)+", " + equation.get(i-1));
					
				double result = Calculate (Double.parseDouble(equation.get(i-2)), equation.get(i), Double.parseDouble(equation.get(i-1)) );	
				System.out.println( "Result is: " + result);
				
				equation.remove(i); //remove operator
				equation.remove(i-1); // remove second number
				String numberAsString = Double.toString(result); // convert the calculation back to to String
				equation.set(i-2, numberAsString); // replace where the first number was with the result
				
				// ----------- display content of the current array ----------------
				System.out.println("---- After ----");
				for (int a=0; a<equation.size(); a++) 
				{
					System.out.print( equation.get(a)+ "|");
				}
				System.out.println("\n");
		   // ----------- end display content of the current array ----------------
		} // end while
			

			gui.display.setText(String.valueOf(Math.round(Double.parseDouble(equation.get(0) ) ) ) );
			equation.clear();
			
			
			
		}
		
}