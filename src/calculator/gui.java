package calculator;

import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

@SuppressWarnings("serial")
public class gui
{
	static TextField display = new TextField("");
	static double memoryStore;

	public gui() 
	{
		Frame CalcFrame = new Frame("Calculator");
		CalcFrame.setLayout(new GridLayout(2,1,0,0));
		CalcFrame.setSize(400,500);
		CalcFrame.setLocation(500, 500);
		CalcFrame.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent evt) 
		 	{
		   		System.exit(0);
		 	}
		});
		
		Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() 
		{
	        public void eventDispatched(AWTEvent event) 
	        {
	        	KeyEvent kEvent = (KeyEvent) event;
	            if(kEvent.getKeyCode() == KeyEvent.VK_ENTER) 
	            {
	                Algo.convert(display.getText() );
	            }
	            else if(kEvent.getKeyCode() == KeyEvent.VK_ESCAPE)
	            {
	            	System.exit(0);
	            }
	            else if(kEvent.getKeyCode() == KeyEvent.VK_BACKSPACE)
	            {
	            	System.exit(0);
	            }
	        }
		}, AWTEvent.KEY_EVENT_MASK);
		
		Panel lcdDisplay = new Panel(); //I/O display
		lcdDisplay.setLayout(new GridLayout(1,1,10,10));	
		
		lcdDisplay.add(display);
		
		CalcFrame.add(lcdDisplay);	
		Panel buttonPanel = new Panel();	
		buttonPanel.setLayout(new GridLayout(5, 5, 0, 0));// (rows, columns, vertical and horizontal space)	
		CalcFrame.add(buttonPanel);	// add the buttons to the Frame
		
		Button memClear = new Button("MC");
		buttonPanel.add(memClear);	
		Button memRead= new Button("MR");
		buttonPanel.add(memRead);
		
		buttonPanel.add(new Button("MS") {{ addActionListener(e -> System.out.println("Memory Stored..") ); }} );
		buttonPanel.add(new Button("<--") {{ addActionListener(e -> display.setText(display.getText().substring(0, display.getText().length()-1  ) )); }} );
		buttonPanel.add(new Button("C") {{ addActionListener(e -> display.setText("")); }} );
		buttonPanel.add(new Button("7") {{ addActionListener(e -> display.setText(display.getText() + "7" )); }} );
		buttonPanel.add(new Button("8") {{ addActionListener(e -> display.setText(display.getText() + "8" )); }} );
		buttonPanel.add(new Button("9") {{ addActionListener(e -> display.setText(display.getText() + "9" )); }} );
		buttonPanel.add(new Button("(") {{ addActionListener(e -> display.setText(display.getText() + "(" )); }} );
		buttonPanel.add(new Button(")") {{ addActionListener(e -> display.setText(display.getText() + ")" )); }} );
		buttonPanel.add(new Button("4") {{ addActionListener(e -> display.setText(display.getText() + "4" )); }} );
		buttonPanel.add(new Button("5") {{ addActionListener(e -> display.setText(display.getText() + "5" )); }} );
		buttonPanel.add(new Button("6") {{ addActionListener(e -> display.setText(display.getText() + "6" )); }} );
		buttonPanel.add(new Button("+") {{ addActionListener(e -> display.setText(display.getText() + "+" )); }} );
		buttonPanel.add(new Button("-") {{ addActionListener(e -> display.setText(display.getText() + "-" )); }} );
		buttonPanel.add(new Button("1") {{ addActionListener(e -> display.setText(display.getText() + "1" )); }} );
		buttonPanel.add(new Button("2") {{ addActionListener(e -> display.setText(display.getText() + "2" )); }} );
		buttonPanel.add(new Button("3") {{ addActionListener(e -> display.setText(display.getText() + "3" )); }} );
		buttonPanel.add(new Button("x") {{ addActionListener(e -> display.setText(display.getText() + "*" )); }} );
		buttonPanel.add(new Button("/") {{ addActionListener(e -> display.setText(display.getText() + "/" )); }} );
		
		Button XXX = new Button("Blank");
		buttonPanel.add(XXX);	

		buttonPanel.add(new Button("0") {{ addActionListener(e -> display.setText(display.getText() + "0" )); }} );
		buttonPanel.add(new Button(".") {{ addActionListener(e -> display.setText(display.getText() + "." )); }} );
		buttonPanel.add(new Button("=") {{ addActionListener(e -> Algo.convert(display.getText() )   ); }} );
		
		CalcFrame.setVisible(true);
	} // end GUI Constructor
} // end Calculator
